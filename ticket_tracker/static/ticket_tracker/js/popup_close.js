function dismissAddRelatedObjectPopup(win, newId, newRepr) {
    var name = windowname_to_id(win.name);
    win.close();
    if (name == 'id_comment') {
        location.reload();
        return
    }

    var elem = document.getElementById(name);
    if (elem) {
        var elemName = elem.nodeName.toUpperCase();
        if (elemName === 'SELECT') {
            elem.options[elem.options.length] = new Option(newRepr, newId, true, true);
        } else if (elemName === 'INPUT') {
            if (elem.className.indexOf('vManyToManyRawIdAdminField') !== -1 && elem.value) {
                elem.value += ',' + newId;
            } else {
                elem.value = newId;
            }
        }
        // Trigger a change event to update related links if required.
        $(elem).trigger('change');
    } else {
        var toId = name + "_to";
        var o = new Option(newRepr, newId);
        SelectBox.add_to_cache(toId, o);
        SelectBox.redisplay(toId);
    }
}
