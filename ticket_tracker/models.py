# encoding: utf-8
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import python_2_unicode_compatible


UserModel = getattr(settings, 'CUSTOM_USER_MODEL', None)
if not UserModel:
    UserModel = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class Attached(models.Model):
    class Meta:
        verbose_name_plural = _("Files")
        verbose_name = _("File")

    content_type = models.ForeignKey(ContentType, verbose_name=_('Type'))
    object_id = models.PositiveIntegerField(_('ID object'))
    content_object = GenericForeignKey('content_type', 'object_id')
    file = models.FileField(_("File"))


@python_2_unicode_compatible
class Comment(models.Model):
    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")

    text = models.TextField(_("Comment"))
    user = models.ForeignKey(UserModel, verbose_name=_("User"), blank=True, null=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(_("Creation date"), auto_now_add=True)
    files = GenericRelation(Attached, verbose_name=_("Files"))

    content_type = models.ForeignKey(ContentType, verbose_name=_('Type'))
    object_id = models.PositiveIntegerField(_('ID object'))
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return '%s <%s>' % (self.created_at, self.user.get_full_name() or self.user )


@python_2_unicode_compatible
class Direction(models.Model):
    class Meta:
        verbose_name = _("Direction")
        verbose_name_plural = _("Directions")

    name = models.CharField(_("Name"), max_length=100, help_text=_("Design|Backend|Frontend..."))

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Sprint(models.Model):
    class Meta:
        verbose_name_plural = _("Sprints")
        verbose_name = _("Sprint")

    STATUSES = (
        (1, _("Planned")),
        (2, _("In work")),
        (3, _("On test")),
        (4, _("Complete")),
        (5, _("Cancel")),
    )

    version = models.CharField(_("Version"), max_length=10)
    description = models.TextField(_("Description"), blank=True, null=True)
    status = models.PositiveSmallIntegerField(_("Status"), choices=STATUSES, default=1)
    files = GenericRelation(Attached, verbose_name=_("Files"), blank=True, null=True)

    def __str__(self):
        return self.version


@python_2_unicode_compatible
class Ticket(models.Model):
    class Meta:
        verbose_name = _("Task")
        verbose_name_plural = _("Tasks")

    TYPES = (
        (1, _("Bug")),
        (2, _("Task")),
        (3, _("Propose")),
        (4, _("Refactor")),
    )

    STATUSES = (
        (1, _("New")),
        (2, _("Appointed")),
        (3, _("In work")),
        (4, _("To test")),
        (5, _("Code review")),
        (6, _("To deploy")),
        (7, _("Complete")),
        (8, _("Duplicate")),
        (9, _("Cancel")),
        (10, _("Delayed"))
    )

    VALID_STATUSES = {
        None: [1],
        1: [1, 2, 8, 9, 10],
        2: [2, 3, 8, 9, 10],
        3: [3, 4, 9, 10],
        4: [4, 2, 5, 9],
        5: [5, 2, 6, 9],
        6: [6, 7, 9, 10],
        7: [7, 2],
        8: [8],
        9: [9, 1],
        10: [10, 1]
    }

    PRIORITY = (
        (1, _("Low")),
        (2, _("Normal")),
        (3, _("High")),
        (4, _("Immediately")),
    )

    name = models.CharField(_("Name"), max_length=100)
    types = models.PositiveSmallIntegerField(_("Type"), choices=TYPES, default=2)
    status = models.PositiveSmallIntegerField(_("Status"), choices=STATUSES, blank=True, null=True)
    priority = models.PositiveSmallIntegerField(_("Priority"), choices=PRIORITY, default=2)
    branch = models.CharField(_("Branch"), max_length=100, blank=True, null=True)
    description = models.TextField(_("Description"), blank=True, null=True)

    created_at = models.DateTimeField(_("Creation date"), auto_now_add=True)
    modified_at = models.DateTimeField(_("Modified date"), auto_now=True)

    created_user = models.ForeignKey(UserModel, verbose_name=_("User who created"), blank=True, null=True,
                                     limit_choices_to={'is_staff': True}, on_delete=models.SET_NULL)
    worker = models.ForeignKey(UserModel, verbose_name=_("Worker"), blank=True, null=True,
                               related_name='workers', on_delete=models.SET_NULL)
    watched = models.ManyToManyField(UserModel, verbose_name=_("Watched users"), blank=True,
                                     related_name='watched_users')

    direction = models.ManyToManyField(Direction, verbose_name=_("Directions"), blank=True)
    files = GenericRelation(Attached, verbose_name=_("Files"), blank=True, null=True)
    sprint = models.ForeignKey(Sprint, verbose_name=_("Sprint"), blank=True, null=True, on_delete=models.PROTECT)
    linked_ticket = models.ForeignKey('self', verbose_name=_("Linked ticket"), blank=True, null=True,
                                      on_delete=models.SET_NULL)

    planned_time = models.DecimalField(_("Planned time"), decimal_places=2, max_digits=10, blank=True, null=True)
    spent_time = models.DecimalField(_("Spent time"), decimal_places=2, max_digits=10, blank=True, null=True)

    def __str__(self):
        return self.name

    def valid_status_choices(self):
        return self.get_valid_ticket_statuses(self.VALID_STATUSES[self.status])

    @staticmethod
    def get_valid_ticket_statuses(list_):
        return (Ticket.STATUSES[i - 1] for i in list_)