# encoding: utf-8
from django import forms
from django.shortcuts import redirect
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.contenttypes.admin import GenericTabularInline
from django.contrib.contenttypes.models import ContentType
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.conf.urls import url
from django.db.models import Sum, TextField, Q
from django.core.urlresolvers import reverse

try:
    from ckeditor_uploader.widgets import CKEditorUploadingWidget
    CKEDITOR = True
except ImportError:
    CKEDITOR = False

try:
    from extended_filters.filters import DateRangeFilter
    CREATED_AT_FILTER = ('created_at', DateRangeFilter)
except ImportError:
    DateRangeFilter = False
    CREATED_AT_FILTER = 'created_at'

from ticket_tracker.models import Sprint, Ticket, Attached, Comment, Direction, UserModel

WORKER_RAW_ID = getattr(settings, 'TICKER_TRACKER_WORKER_RAW_ID', True)
STATUS_CHOICES = getattr(settings, 'TICKET_TRACKER_STATUS_CHOICES', [])
ALLOW_DELETE = getattr(settings, 'TICKET_TRACKER_ALLOW_DELETE', [])


def get_username(user):
    return getattr(user, user._meta.model.USERNAME_FIELD)


def delete_is_allowed(request, obj):
    return ALLOW_DELETE or request.user.pk in ALLOW_DELETE or get_username(request.user) in ALLOW_DELETE


# Register your models here.
class AttachedInline(GenericTabularInline):
    model = Attached
    extra = 1

    def has_delete_permission(self, request, obj=None):
        return delete_is_allowed(request, obj)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):

    list_display = ['user', 'created_at']
    inlines = [AttachedInline]

    class form(forms.ModelForm):
        def __init__(self, *args, **kwargs):
            super(CommentAdmin.form, self).__init__(*args, **kwargs)
            self.fields['user'].widget = forms.HiddenInput()
            self.fields['content_type'].widget = forms.HiddenInput()
            self.fields['object_id'].widget = forms.HiddenInput()
            if CKEDITOR:
                self.fields['text'].widget = CKEditorUploadingWidget()

    def get_changeform_initial_data(self, request):
        initial = super(CommentAdmin, self).get_changeform_initial_data(request)
        initial['user'] = request.user
        initial['content_type'] = ContentType.objects.get_by_natural_key('ticket_tracker', initial['content_type'])
        return initial

    def has_delete_permission(self, request, obj=None):
        return delete_is_allowed(request, obj)


class CommentInline(GenericTabularInline):
    model = Comment
    readonly_fields = ['from_who', 'get_text', 'get_files']
    ordering = ['-created_at']
    fields = ['from_who', 'get_text', 'get_files']

    def get_queryset(self, request):
        return super(CommentInline, self).get_queryset(request).prefetch_related('files')

    def has_add_permission(self, request):
        return

    def has_delete_permission(self, request, obj=None):
        return delete_is_allowed(request, obj)

    def from_who(self, obj):
        return str(obj)
    from_who.short_description = 'От кого'

    def get_text(self, obj):
        return format_html(obj.text)
    get_text.short_description = _('Comment')

    def get_files(self, obj):
        return format_html('<br>'.join('<a href="%s">%s</a>' % (i.file.url, i.file.name) for i in obj.files.all()))
    get_files.short_description = _('Files')


@admin.register(Sprint)
class SprintAdmin(admin.ModelAdmin):
    class Media:
        js = ('js/popup_close.js',)
        css = {
            'all': ('css/styles.css',)
        }

    change_form_template = 'ticket_tracker/change_form.html'
    list_display = ['version', 'status', 'sum_planned_time', 'sum_spent_time']
    list_filter = ['status']
    inlines = [AttachedInline, CommentInline]

    class form(forms.ModelForm):
        class Meta:
            model = Sprint
            fields = '__all__'

        tickets = forms.ModelMultipleChoiceField(queryset=Ticket.objects.none(),
                                                 required=False,
                                                 label=_("Tickets"),
                                                 widget=FilteredSelectMultiple("Tickets", False))

        def __init__(self, *args, **kwargs):

            super(SprintAdmin.form, self).__init__(*args, **kwargs)
            if self.instance.id:
                self.fields['tickets'].initial = self.instance.ticket_set.all()
                self.fields['tickets'].queryset = Ticket.objects.filter(Q(sprint__isnull=True) |
                                                                        Q(sprint=self.instance))
            else:
                self.fields['tickets'].widget = forms.HiddenInput()

        def save(self, commit=True):
            instance = super(SprintAdmin.form, self).save(commit=False)
            if instance.id:
                self.fields['tickets'].initial.update(sprint=None)
                self.cleaned_data['tickets'].update(sprint=instance)
            return super(SprintAdmin.form, self).save()

    def get_queryset(self, request):
        return super(SprintAdmin, self).get_queryset(request).annotate(
            planned_time=Sum('ticket__planned_time'),
            spent_time=Sum('ticket__spent_time')
        )

    def sum_planned_time(self, obj):
        return obj.planned_time
    sum_planned_time.short_description = _("Sum of planned time")

    def sum_spent_time(self, obj):
        return obj.spent_time
    sum_spent_time.short_description = _("Sum of spent time")

    def has_delete_permission(self, request, obj=None):
        return delete_is_allowed(request, obj)


@admin.register(Ticket)
class TicketAdmin(admin.ModelAdmin):
    class Media:
        js = ('js/popup_close.js',)
        css = {
            'all': ('css/styles.css',)
        }

    change_form_template = 'ticket_tracker/change_form.html'
    list_display = ['name', 'modified_at', 'types', 'status', 'priority',  'worker', 'get_directions', 'sprint']
    list_filter = ['sprint', 'types', 'status', 'priority', CREATED_AT_FILTER,
                   ('created_user', admin.RelatedOnlyFieldListFilter),
                   ('worker', admin.RelatedOnlyFieldListFilter),
                   ('direction', admin.RelatedOnlyFieldListFilter)]
    inlines = [AttachedInline, CommentInline]
    exclude = ['created_user', 'watched']
    ordering = ['-created_at']
    filter_horizontal = ['direction']
    readonly_fields = ['get_linked_tickets', 'created_user', 'watched', 'created_at', 'modified_at']
    fields = (
        ('name', 'types', 'status', 'priority'),
        ('sprint', 'worker',),
        ('linked_ticket', 'get_linked_tickets'),
        ('planned_time', 'spent_time'),
        ('description',),
        ('direction',),
        ('created_user', 'created_at', 'modified_at'),
        ('watched',),
    )
    actions = ['add_ticket_for_watching', 'remove_ticket_from_watching']
    if CKEDITOR:
        formfield_overrides = {
            TextField: {'widget': CKEditorUploadingWidget()}
        }
    raw_id_fields = ['linked_ticket']
    if WORKER_RAW_ID:
        raw_id_fields += ['worker']

    def has_delete_permission(self, request, obj=None):
        return delete_is_allowed(request, obj)

    def get_queryset(self, request):
        return super(TicketAdmin, self).get_queryset(request).prefetch_related('direction', 'ticket_set')

    def get_directions(self, obj):
        return ', '.join([o.name for o in obj.direction.all()])
    get_directions.short_description = _('Directions')

    def save_form(self, request, form, change):
        try:
            form.instance.created_user
        except self.model._meta.apps.get_model(UserModel).DoesNotExist:
            form.instance.created_user = request.user
        return super(TicketAdmin, self).save_form(request, form, change)

    def get_linked_tickets(self, obj):
        if obj.ticket_set.exists():
            return format_html(
                '<br>'.join(
                    ['<a href="%s">%s<a>' %
                     (reverse('admin:ticket_tracker_ticket_change', args=[t.id]), str(t)) for t in obj.ticket_set.all()]
                )
            )
        return ''
    get_linked_tickets.short_description = _('All linked tickets')

    def get_urls(self):
        urls = [
            url(r'add-watching/(\d+)/$', self.add_watch_ticket_view, name='add_ticket_for_watching'),
            url(r'remove-watching/(\d+)/$', self.remove_watch_ticket_view, name='remove_ticket_from_watching')
        ]
        return urls + super(TicketAdmin, self).get_urls()

    def add_watch_ticket_view(self, request, object_id):
        self.add_ticket_for_watching(request, Ticket.objects.filter(id=object_id))
        return redirect(reverse('admin:ticket_tracker_ticket_change', args=object_id))

    def remove_watch_ticket_view(self, request, object_id):
        self.remove_ticket_from_watching(request, Ticket.objects.filter(id=object_id))
        return redirect(reverse('admin:ticket_tracker_ticket_change', args=object_id))

    def add_ticket_for_watching(self, request, queryset):
        for ticket in queryset:
            ticket.watched.add(request.user)
        self.message_user(request, _("Tickets was added for watching"))
    add_ticket_for_watching.short_description = _("Watch for ticket")

    def remove_ticket_from_watching(self, request, queryset):
        for ticket in queryset:
            ticket.watched.remove(request.user)
        self.message_user(request, _("Ticket was remove from watching"))
    remove_ticket_from_watching.short_description = _("Remove ticket from watching")

    def get_form(self, request, obj=None, **kwargs):
        # Hook for add user to admin form
        form = super(TicketAdmin, self).get_form(request, obj, **kwargs)
        form.user = request.user
        return form

#    class form(forms.ModelForm):
#        class Meta:
#            model = Ticket
#            fields = '__all__'
#
#        def __init__(self, *args, **kwargs):
#            super(TicketAdmin.form, self).__init__(*args, **kwargs)
#            choices = self.instance.valid_status_choices()
#            if STATUS_CHOICES == '__all__' or self.user.pk in STATUS_CHOICES or\
#                            get_username(self.user) in STATUS_CHOICES:
#                choices = Ticket.STATUSES
#            self.fields['status'].choices = choices
#
#        def clean(self):
#            status = self.cleaned_data['status']
#            if status in (2, 3) and not self.cleaned_data.get('worker'):
#                raise forms.ValidationError(_("You must to choose worker if you change status to appointed"))
#            return self.cleaned_data


@admin.register(Direction)
class DirectionAdmin(admin.ModelAdmin):
    list_display = ['name']