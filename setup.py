from setuptools import setup, find_packages


setup(
    name='django-ticket-tracker',
    version='0.2.1',
    url='https://bitbucket.org/legion_an/django-ticket-tracker',
    packages=find_packages(),
    include_package_data=True,
    license='',
    author='legion',
    author_email='legion_an@mail.ru',
    description='Add ticket tracker in admin',
    keywords=[
        'tickets tracker',
        'issues tracker',
    ],
    install_requires=[
        "django",
    ],
)
