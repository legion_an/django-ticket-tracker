# Add 'ticket_tracker' to INSTALLED_APPS!
```
#!python

INSTALLED_APPS = (
    ...
    'models_logging',
    ...
)


TICKER_TRACKER_WORKER_RAW_ID = False    # for simple select, by default raw_id_field

# ticket statuses has a certain order. 
# if you want to choice any status set it = '__all__' or add usernames, ids that can choice any status
TICKET_TRACKER_STATUS_CHOICES = '__all__'/['user', 25]    
       
       
# By default you can't delete anythings, but you can set it True or add usernames, ids that can delete trackers things
TICKET_TRACKER_ALLOW_DELETE = True/['user', 25]
```
# make migrations
# Go to django admin